const validSizes = ['XS', 'S', 'M', 'L', 'XL'];
const constants = {
    validSizes
};
export default constants;