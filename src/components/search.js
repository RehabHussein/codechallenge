import React from 'react';
import classes from './search.module.css';

const Search = (props) => {
    const handleInputChange = (e) => {
        if (e.key === 'Enter') {
            //props.onFilterProducts(e.target.value);
            props.history.push(`/search/${e.target.value}`);
        }
    }
    return (
        <input
            className={classes.input}
            //value={}
            onKeyUp={handleInputChange}
            placeholder={'Search'}></input>
    );
}
export default Search;