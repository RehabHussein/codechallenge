import React from 'react';
import Grid from '@material-ui/core/Grid';
import classes from './category.module.css';
import apis from '../services/services';

const Category = (props) => {
    const { categoryItem, history } = props;
    return (
        <div className={classes.container} onClick={()=>{history.push(`/category/${categoryItem.category_key}/${categoryItem.name}`)}}>
            <h2 className={classes.name}>{categoryItem.name}</h2>
            <img className={classes.img} src={categoryItem.category_image} />
        </div>
    );
}

const CategoryList = (props) => {
    const [categories, setCategories] = React.useState([]);
    const [error, setError] = React.useState('');
    React.useEffect(() => {
        apis.fetchCategories()
            .then(categories => setCategories(categories))
            .catch(() => setError('An Error Occured, please try again later'))
    }, []);
    return (

        <Grid container spacing={2}>
            {categories.length > 0 ? categories.map(categoryItem => {
                return <Category key={categoryItem.key} categoryItem={categoryItem} history={props.history}/>
            }) : error==='' ?  
            <h2>No categories are avaiable at the moment!</h2>
            :  <h2>{error}</h2>}
        </Grid>
    );
}
export default CategoryList;