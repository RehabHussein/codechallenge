import React, { useState } from 'react';
import classes from './product.module.css';
import { ReactComponent as FavIcon } from '../assets/Shape.svg';
import { ReactComponent as NotFavIcon } from '../assets/NotFav.svg';
import apis from '../services/services';
import Grid from '@material-ui/core/Grid';


const Product = (props) => {
    const { product } = props;
    const isItemFavorite = () => {
        return sessionStorage.getItem("favoriteList").split('##newItem##').filter(item => item == product.id).length > 0
    };
    const [isFav, setIsFav] = React.useState(isItemFavorite());
    const handleFavIconClick = (isAddToFavoriteItem) => {
        // add item to session storage
        if (isAddToFavoriteItem)
            sessionStorage.setItem("favoriteList", sessionStorage.getItem("favoriteList") + '##newItem##' + product.id);
        else
            sessionStorage.setItem("favoriteList", sessionStorage.getItem("favoriteList").split('##newItem##').filter(item => item != product.id).join('##newItem##'));
        setIsFav(!isFav);
    }
    return (
        <div className={classes.container}>
            <img src={product.image} className={classes.img} alt={product.name} />
            <div className={classes.contentContainer}>
                <h4>{product.name}</h4>
                <h5>{product.price}$</h5>
            </div>
            <div className={classes.badgeContainer}>
                {isFav ? <FavIcon className={classes.badge} fill='red' onClick={() => handleFavIconClick(false)} />
                    :
                    <NotFavIcon className={classes.badge} onClick={() => handleFavIconClick(true)} />
                }
            </div>
        </div>
    );
};

const ProductList = (props) => {
    const [products, setProducts] = useState([]);
    const [error, setError] = useState('');
    React.useEffect(() => {
        const params = new Map();
        props.match.params.id && params.set("category", props.match.params.id);
        props.match.params.text && params.set("text", props.match.params.text);
        props.match.params.filter && params.set("size", props.match.params.filter);
        setError('');
        apis.fetchProducts(params)
            .then(products => setProducts(products))
            .catch(() => setError('An Error Occured, please try again later'))
    }, []);
    return (
        <Grid container spacing={2}>
            {products.length > 0 ?
                products.map(product => <Grid item className={classes.gridItem} xs={3}><Product key={product.name} product={product} /></Grid>)
                : error === '' ?
                    <h2>No products are avaiable at the moment, check the other categories, we have awesome products there !</h2>
                    : <h2>{error}</h2>
            }

        </Grid>
    );
}

export default ProductList;