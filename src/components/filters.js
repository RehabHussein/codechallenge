import React, { useState } from 'react';
import { Box, Button } from '@material-ui/core';
import constants from '../constants';
import classes from './filters.module.css';

const FilterButton = (props) => {
    const handleFilterOpen = () => {
        props.history.push('/filters')
    }
    return (<Button
        color="primary"
        size="small"
        onClick={handleFilterOpen}>
        Filters
    </Button>)

};
export const Filters = (props) => {

    const [selectedSizes, setSelectedSizes] = useState([]);
    const handleApply = () => {
        props.history.push(`/filter/${selectedSizes.join(',')}`);
    }
    const handleDiscard = () => {
        props.history.pop();
    }
    const handleSizeSelection = (size) => {
        const indexOfSize = selectedSizes.indexOf(size);
        indexOfSize == -1 ? setSelectedSizes([...selectedSizes, size]) :
            setSelectedSizes(selectedSizes.filter(addedSize => addedSize !== size));
    }
    return (
        <Box className={classes.container}>
            <div>
                <h2> Filters</h2>
                <p className={classes.section}>Sizes</p>
                {constants.validSizes.map((size) => {
                    return (<Button
                        className={classes.actionBtn}
                        variant={selectedSizes.indexOf(size) == -1 ? 'outlined' : 'contained'}
                        size="small"
                        color="primary"
                        onClick={() => handleSizeSelection(size)}>{size}</Button>);
                })}
            </div>
            <div className={classes.actionButtonsContainer}>
                <Button className={classes.actionBtn} variant="contained" color="secondary" onClick={handleDiscard}>Discard</Button>
                <Button className={classes.actionBtn} variant="contained" color="primary" onClick={handleApply}>Apply</Button>
            </div>
        </Box>
    )

};
export default FilterButton;