import React from 'react';
import logo from './logo.svg';
import './App.css';
import CategoryList from './components/category';
import Search from './components/search';
import { Route, Switch, useHistory } from 'react-router-dom';
import ProductList from './components/product';
import FilterButton, { Filters } from './components/filters';

function App() {
  return (
    <div className="App">
      <Switch>
        <Route path='/' component={(props) => (
          <div>
            <h1 className="header">Shop</h1>
            <div style={{ display: 'flex' }}>
              <Search {...props} />
              <FilterButton {...props} />
            </div>
            <CategoryList
              {...props} />
          </div>

        )} exact />
        <Route path="/category/:id/:name" component={(props) =>
          <>
            <h1 className="header">{props.match.params.name}</h1>
            <div style={{ display: 'flex', justifyContent: 'flex-end', marginBottom: '1rem' }}>
              <FilterButton {...props} />
            </div>
            <ProductList {...props} />
          </>
        } />
        <Route path="/search/:text" component={(props) => <ProductList {...props} />} />
        <Route path="/filter/:filter" component={(props) => <ProductList {...props} />} />
        <Route path="/filters" component={(props) => <Filters {...props} />} />
      </Switch>
    </div>
  );
}

export default App;
