function fetchProducts(params) {
    const myUrlWithParams = new URL("http://localhost:3001/products");
    params.forEach((value, key) => {
        myUrlWithParams.searchParams.append(key, value);
    });

    return fetch(myUrlWithParams)
        .then((result) => result.json())
        .then(data => data)
}
function fetchCategories() {
    return fetch(`http://localhost:3001/categories`)
        .then((result) => result.json())
        .then(data => data)
}

const apis = {
    fetchProducts,
    fetchCategories
};
export default apis;