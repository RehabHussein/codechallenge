This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `Cases that are covered in the app`
1. User can navigate between all different categories, select one category and navigate between different products
2. User can add on product to his favorites and can find it even after refreshing the browser
3. User can filter the products based on the sizes, or any search text in the search text area
4. Handling network error with an error message
5. Handling empty results with a user friendly message 

### `Things to be improved`
1. Adding some tests using Jest
2. Moving all the static values to the constants.js file
3. For consistent design, using material ui elements, or any other design system components for all the UI in the app as the UI library
4. Implementing HOC or custom hook for handling loading/error of all api calls
5. Improving the search to be case insensitive
6. Adding fallback image for any broken image from the server




